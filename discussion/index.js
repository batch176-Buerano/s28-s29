//[OBJECTIVE] Create a server-side app using Express Web Framework.


//Relate this task to something that you do on a daily basis.

//[SSECTION]append the intire app to our node package manager
	//package.json -> the heart of every node projects. this also contains different metadata that describes the structure of the project.

	//sprits -> is used to declear and discribe custom commands and keyword that can be used to execute this project with the correct runtime environment.

	//NOTE: "start" is globally recognize amongst node project and frameworks as the 'defult' command script to execute a task/project.
	//however for *uncinventional* keyword or command you have to append the command "run"
	//Syntax: npm run <custom command>

//1. Identify and prepare the ingredients.
const express = require("express");

//express => will be used as the main component to create the server.
//we need to be able to gather/acquaire the utilities and componets needed that the express librart will provide us.
	//require() -> directive used to get the library components needed inside the module.

	//prepare the invironment in which the project will be sarved.

//[SECTION] Preparing a Remote respository for our Node project.
	//NOTE: always DISABLE the node_modules folder
	//WHY? 
		//-> it will take up to much space in our repository making a lot more difficult to stage upon commiting the changes in our remote repo.
		//-> if ever that you will deploy your bide project on deploying platforms (heroku, netlify, vercel) the project will automatically be rejected because node_module is not recognized on various deployment platforms.

//[SECTION] Create a Runtime environment that automaticaly autofix all the changes in out app.

	//were going to use a utility called nodemon.
	//upon starting the entry point module with nodemon will be able to the 'append' the application with the proper runtime and effort upon commiting change to your app.
// console.log('This will be our server');
// console.log('NEW CHANGES');
// console.log('Change');
//you can even insert item like text art into your run time environment.
console.log(`
	Welcome to our Express API server
	▄───▄
	█▀█▀█
	█▄█▄█
	─███──▄▄
	─████▐█─█
	─████───█
	─▀▀▀▀▀▀▀
	`);